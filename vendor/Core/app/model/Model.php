<?php

namespace Core\app\model;

abstract class Model
{
    private $mysqli;
    private $query;

    public function __construct()
    {
        $this->mysqli = new \mysqli('127.0.0.1', 'root', '', 'testtask');


        if ($this->mysqli->connect_errno) {
            echo "Error code: " . $this->mysqli->connect_errno . "\n";
            echo "Error: " . $this->mysqli->connect_error . "\n";

            exit;
        }
    }


    public function insertOrUpdate(array $data)
    {
        $query = $this->mysqli->query("
            CREATE TABLE IF NOT EXISTS `user` (
              `github_id` int(11) UNSIGNED NOT NULL,
              `github_login` varchar(255) NOT NULL,
              PRIMARY KEY (github_id)
            ) ");
        $key = implode(', ', array_keys($data));
        $val = implode(', ', array_values($data));
        $query = $this->mysqli->query(
            "INSERT INTO `user` (`github_id`, `github_login`) 
                    VALUES ('" . $data['github_id'] . "', '" . $data['github_login'] . "')
                     ON DUPLICATE KEY UPDATE `github_login`='" . $data['github_login'] . "'");
        echo $this->mysqli->error;
    }
}