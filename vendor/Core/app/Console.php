<?php
namespace Core\App;

use Core\App\http\Router;

class Console
{
    private $args;

    public function __construct($argv)
    {
        $this->args = $argv;

        switch ($this->args[1]){
            case 'update': (new http\Router)->route('https://api.github.com/users', 'UserController@update');
        }
    }
}