<?php
namespace Core\App\http;

use UserController;

class Router
{


    public function route($url, $controller){
        $controller = explode('@', $controller);
        $namespace = 'App\http\controllers\\'.$controller[0];
        $class = new $namespace();
        $method = $controller[1];
        $class->$method($url);


    }

}