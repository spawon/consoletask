<?php


namespace App\http\controllers;


use App\models\User;
use Core\app\http\Controller;

class UserController extends Controller
{
    public function update($url)
    {
           $opts = [
            'http' => [
                'method' => 'GET',
                'header' => [
                    'User-Agent: PHP'
                ]
            ]
        ];
        $context = stream_context_create($opts);
        $data = file_get_contents($url, false, $context);

        $data = json_decode($data);
        $user = new User();
        foreach ($data as $datum) {
            $user->insertOrUpdate([
                'github_id' => $datum->id,
                'github_login' => $datum->login,
            ]);
        }




    }
}